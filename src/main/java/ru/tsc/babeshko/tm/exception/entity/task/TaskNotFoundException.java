package ru.tsc.babeshko.tm.exception.entity.task;

import ru.tsc.babeshko.tm.exception.entity.AbstractEntityNotFoundException;

public final class TaskNotFoundException extends AbstractEntityNotFoundException {

    public TaskNotFoundException() {
        super("Error! Task not found...");
    }

}
