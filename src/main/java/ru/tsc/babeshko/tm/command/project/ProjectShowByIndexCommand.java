package ru.tsc.babeshko.tm.command.project;

import ru.tsc.babeshko.tm.model.Project;
import ru.tsc.babeshko.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    public static final String NAME = "proj-show-by-index";

    public static final String DESCRIPTION = "Show project by index.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = getUserId();
        final Project project = getProjectService().findOneByIndex(userId, index);
        showProject(project);
    }

}