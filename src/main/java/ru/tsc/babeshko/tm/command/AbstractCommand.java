package ru.tsc.babeshko.tm.command;

import ru.tsc.babeshko.tm.api.model.ICommand;
import ru.tsc.babeshko.tm.api.service.IAuthService;
import ru.tsc.babeshko.tm.api.service.IServiceLocator;
import ru.tsc.babeshko.tm.enumerated.Role;

public abstract class AbstractCommand implements ICommand {

    public abstract String getName();

    public abstract String getDescription();

    public String getArgument() {
        return null;
    }

    public abstract Role[] getRoles();

    public IAuthService getAuthService() {
        return getServiceLocator().getAuthService();
    }

    public String getUserId(){
        return getAuthService().getUserId();
    }

    public abstract void execute();

    protected IServiceLocator serviceLocator;

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        String result = "";
        if (name != null && !name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

}
