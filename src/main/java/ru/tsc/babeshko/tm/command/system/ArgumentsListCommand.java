package ru.tsc.babeshko.tm.command.system;


import ru.tsc.babeshko.tm.api.model.ICommand;
import ru.tsc.babeshko.tm.command.AbstractCommand;

import java.util.Collection;

public final class ArgumentsListCommand extends AbstractSystemCommand {

    public static final String NAME = "argument";

    public static final String DESCRIPTION = "Show argument list.";

    public static final String ARGUMENT = "-arg";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final ICommand command : commands) {
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

}
