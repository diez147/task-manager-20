package ru.tsc.babeshko.tm.api.service;

import ru.tsc.babeshko.tm.api.repository.IUserOwnedRepository;
import ru.tsc.babeshko.tm.model.AbstractUserOwnedModel;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M> {

}
