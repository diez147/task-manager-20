package ru.tsc.babeshko.tm.api.service;

import ru.tsc.babeshko.tm.enumerated.Role;
import ru.tsc.babeshko.tm.model.User;


public interface IUserService extends IService<User>{

    User removeByLogin(String login);

    User findByEmail(String email);

    User findByLogin(String login);

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User setPassword(String userId, String password);

    User updateUser(String userId, String firstName, String lastName, String middleName);

    boolean isLoginExist(String login);

    boolean isEmailExist(String email);

}