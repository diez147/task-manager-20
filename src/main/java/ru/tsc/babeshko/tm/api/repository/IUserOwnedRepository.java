package ru.tsc.babeshko.tm.api.repository;

import ru.tsc.babeshko.tm.enumerated.Sort;
import ru.tsc.babeshko.tm.model.AbstractUserOwnedModel;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {


    void clear(String userId);

    List<M> findAll(String userId);

    List<M> findAll(String userId, Sort sort);

    List<M> findAll(String userId, Comparator comparator);

    boolean existsById(String userId, String id);

    M findOneById(String userId, String id);

    M findOneByIndex(String userId, Integer index);

    M removeById(String userId, String id);

    M removeByIndex(String userId, Integer index);

    int getSize(String userId);

    M add(final String userId, M model);

    M remove(final String userId, M model);


}
