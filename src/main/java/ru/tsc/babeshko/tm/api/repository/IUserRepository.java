package ru.tsc.babeshko.tm.api.repository;

import ru.tsc.babeshko.tm.model.User;

public interface IUserRepository extends IRepository<User>{

    User findByEmail(String email);

    User findByLogin(String login);

    User removeByLogin(String login);

}